const clickButton = (clickedId) => {
    _removeClickedClass('.day');

    const dayName = document.getElementById(clickedId);

    dayName.classList.add("clicked");
    document.getElementById('selected-day').innerHTML = dayName.innerHTML;
};

const _removeClickedClass = (theClass) => {
    const allClasses = document.querySelectorAll(theClass);

    allClasses.forEach((thisClass) => {
        if (thisClass.classList.contains('clicked')) {
            thisClass.classList.remove('clicked');
        }
    })
}