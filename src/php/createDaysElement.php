<?php
function createDaysHeader() {
    $day = date('l');
    echo "<div class='day-title'>Today is <span id='the-day'>$day</span></div>";
}

function createDaysBlock($days) {
    foreach ($days as $day) {
        if ($day == date('l')) {
            echo "<div class='day now' id='$day' onClick='clickButton(this.id);'>$day</div>";
        } else {
            echo "<div class='day' id='$day' onClick='clickButton(this.id);'>$day</div>";
        }
    }
}