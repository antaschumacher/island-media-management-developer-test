<?php include './resources/days.php'; ?>
<?php include './src/php/createDaysElement.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="./styles/style.css" type="text/css">
    <title>Developer Test</title>
</head>

<body>
    <div class="header">
        <?php createDaysHeader(); ?>
    </div>
    <div class="days">
        <?php createDaysBlock($days); ?>
    </div>
    <div class="footer">
        <p class="selected-title">Selected day is <span id='selected-day'>...</span></p>
    </div>

    <script src="./src/js/clickButton.js"></script>
</body>
</html>